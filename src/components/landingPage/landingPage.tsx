import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { useMoviesQuery } from "../../api/movieService";
import { statusEnum } from "../../interfaces/Service";
import GridView from "../GridView";
import TableView from "../TableView";

const LandingPage: React.FC = ({}) => {
  const [viewTable, setView] = useState(false);
  const service = useMoviesQuery();

  function toggleView() {
    setView((viewTable) => !viewTable); //functional update
  }

  const btn = <Button onClick={toggleView}>Change view</Button>;

  if (service.status === statusEnum.loading) {
    return (
      <>
        {btn}
        <h2>Loading...</h2>
      </>
    );
  } else if (service.status === statusEnum.loaded && viewTable) {
    return (
      <>
        {btn}
        <TableView movies={service.payload} />
      </>
    );
  } else if (service.status === statusEnum.loaded && !viewTable) {
    return (
      <>
        {btn}
        <GridView movies={service.payload} />
      </>
    );
  } else {
    return <p>???</p>;
  }
};

export default LandingPage;
