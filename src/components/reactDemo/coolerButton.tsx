import React from "react";

interface ICoolerButton {
  buttonText: string;
  clickHandler: () => void;
}

const CoolerButton = ({ buttonText, clickHandler }: ICoolerButton) => {
  return (
    <>
      <button onClick={clickHandler}>{buttonText}</button>
    </>
  );
};

export default CoolerButton;
