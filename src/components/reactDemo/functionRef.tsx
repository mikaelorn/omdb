import React, { useEffect, useState } from "react";
import { getServerData } from "../../api/movieService";

interface IFunctionalReactDemoProps {}

const FunctionalReactDemo = ({}: IFunctionalReactDemoProps) => {
  const [counter, setCounter] = useState(0);
  const [serverData, setServerData] = useState("Loading...");

  useEffect(() => {
    getServerData().then((data) => {
      setServerData(data);
    });
  }, []);

  function increment() {
    setCounter(counter + 1);
  }
  function decrement() {
    setCounter(counter - 1);
  }

  return (
    <div>
      <div>Hello react!</div>
      <button onClick={increment}>+</button>
      <div>Counter: {counter}</div>
      <button onClick={decrement}>-</button>
      <div>from server: {serverData}</div>
    </div>
  );
};

export default FunctionalReactDemo;
