import React from "react";
import { getServerData } from "../../api/movieService";
import CoolerButton from "./coolerButton";

interface IReactDemoProps {}
interface IReactDemoState {
  counter: number;
  apiData: string;
}

class ReactDemo extends React.Component<IReactDemoProps, IReactDemoState> {
  constructor(props: IReactDemoProps) {
    super(props);
    this.state = {
      counter: 0,
      apiData: "Loading...",
    };

    //bind creates new function with this keyword set to provided val. so we can access components this in
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
  }

  async componentDidMount() {
    let serverData = await getServerData();
    this.setState({ apiData: serverData });
  }

  increment() {
    this.setState({ counter: this.state.counter + 1 });
  }
  decrement() {
    this.setState({ counter: this.state.counter - 1 });
  }

  render() {
    return (
      <div>
        <div>Hello react!</div>
        <CoolerButton buttonText="+" clickHandler={this.increment} />
        <div>Counter: {this.state.counter}</div>
        <CoolerButton buttonText="-" clickHandler={this.decrement} />
        <h3>Server data: {this.state.apiData}</h3>
      </div>
    );
  }
}

export default ReactDemo;
