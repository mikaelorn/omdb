import React from "react";
import { Movie } from "../../interfaces/Movie";
import MovieTile from "../movieTile";

const gridContainerStyle = {
  display: "grid",
  gridTemplateColumns: "188px 188px 188px 188px",
  width: "700px",
};

interface IGridView {
  movies: Array<Movie>;
}

const GridView: React.FC<IGridView> = ({ movies }) => {
  return (
    <div style={gridContainerStyle}>
      {movies.map((movie) => (
        <MovieTile movie={movie} />
      ))}
    </div>
  );
};

export default GridView;
