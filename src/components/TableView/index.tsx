import React from "react";
import { Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { Movie } from "../../interfaces/Movie";

interface ITableView {
  movies: Array<Movie>;
}

const TableView: React.FC<ITableView> = ({ movies }) => {
  const history = useHistory();
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Title</th>
          <th>Genre</th>
          <th>Rating</th>
        </tr>
      </thead>
      <tbody>
        {movies.map((movie) => {
          return (
            <tr onClick={() => history.push("/edit-movie/" + movie.id)}>
              <td>{movie.id}</td>
              <td>{movie.name}</td>
              <td>{movie.type}</td>
              <td>{movie.rating}</td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default TableView;
