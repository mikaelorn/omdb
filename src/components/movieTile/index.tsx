import React from "react";
import { useHistory } from "react-router-dom";
import { Movie } from "../../interfaces/Movie";
import StarRating from "../common/StarRating";

const gridItemStyle = {
  width: "188px",
  height: "300px",
  cursor: "pointer",
};

const movieTileImage = {
  width: "100%",
};

interface IMovieTile {
  movie: Movie;
}

const MovieTile = ({ movie }: IMovieTile) => {
  let history = useHistory();

  const handleItemClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    history.push(`/edit-movie/${movie.id}`);
  };

  return (
    <div style={gridItemStyle} onClick={handleItemClick}>
      <img style={movieTileImage} src={movie.imageUrl} />
      <StarRating rating={movie.rating} />
    </div>
  );
};

export default MovieTile;
