import React from "react";
import MovieForm from "../common/movieForm/MovieForm";
import { Movie } from "../../interfaces/Movie";
import { useHistory } from "react-router-dom";
import { createMovie } from "../../api/movieService";

const CreateMovie = () => {
  const history = useHistory();
  async function handleSubmit(movie: Movie) {
    await createMovie(movie);
    history.push("/");
  }

  return <MovieForm submit={handleSubmit} />;
};

export default CreateMovie;
