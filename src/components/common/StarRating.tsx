import React, { useState } from "react";
import { Star, StarFill } from "react-bootstrap-icons";

interface IStarRating {
  rating: number;
  onChange?: (rating: number) => void;
}

const StarRating: React.FC<IStarRating> = ({ rating, onChange }) => {
  const [selectedStars, setSelectedStars] = useState(rating);
  const handleClick = (i: number) => {
    onChange && onChange(i + 1);
    setSelectedStars(i + 1);
  };
  const stars = [];
  for (let i = 0; i < 5; i++) {
    stars.push(
      i < selectedStars ? (
        <StarFill key={i} onClick={() => handleClick(i)} />
      ) : (
        <Star key={i} onClick={() => handleClick(i)} />
      )
    );
  }
  return <>{stars}</>;
};

export default StarRating;
