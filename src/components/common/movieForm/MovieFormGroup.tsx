import { Field } from "formik";
import React from "react";

interface IMovieFormControl {
  type: string;
  name: string;
  errors?: any;
  touched?: any;
  value: any;
}

const MovieFormGroup: React.FC<IMovieFormControl> = (props) => {
  return (
    <>
      <Field
        name={props.name}
        value={props.value}
        className={
          props.name && props.errors
            ? "form-control is-invalid"
            : "form-control"
        }
        type={props.type}
      />
      {props.touched && props.errors ? (
        <div className="invalid-feedback">{props.errors}</div>
      ) : null}
    </>
  );
};

export default MovieFormGroup;
