import React, { useState } from "react";
import { Form, Formik, FormikProps } from "formik";
import {
  Movie,
  initialMovieValues,
  movieSchema,
} from "../../../interfaces/Movie";
import { Col, Form as BootstrapForm } from "react-bootstrap";
import MovieFormGroup from "./MovieFormGroup";
import StarRating from "../StarRating";

interface IMovieForm {
  submit: (values: Movie) => void;
  movie?: Movie;
}

const MovieForm: React.FC<IMovieForm> = ({ submit, movie }) => {
  const [rating, setRating] = useState(movie ? movie.rating : 0);
  const movieValues = !movie ? initialMovieValues : movie;

  const handleFormSubmit = (values: Movie) => {
    console.log(values);
    submit(values);
  };

  console.log("movieValues: " + JSON.stringify(movieValues));

  return (
    <Formik
      initialValues={movieValues}
      onSubmit={(values) => {
        debugger;
        values.rating = rating;
        handleFormSubmit(values);
      }}
      validationSchema={movieSchema}
    >
      {(formik: FormikProps<Movie>) => (
        <Form onSubmit={formik.handleSubmit}>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={4}>
              <label htmlFor="name">Name</label>
              <MovieFormGroup
                name="name"
                value={formik.values.name}
                type="text"
                touched={formik.touched.name}
                errors={formik.errors.name}
              />
            </BootstrapForm.Group>
            <BootstrapForm.Group as={Col} md={4}>
              <label htmlFor="type">Genre</label>
              <MovieFormGroup
                name="type"
                value={formik.values.type}
                type="text"
                touched={formik.touched.type}
                errors={formik.errors.type}
              />
            </BootstrapForm.Group>
            <label style={{ display: "block" }}>Rating</label>
            <StarRating rating={formik.values.rating} onChange={setRating} />
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={4}>
              <label htmlFor="year">Year</label>
              <MovieFormGroup
                name="year"
                value={formik.values.year}
                type="number"
                touched={formik.touched.year}
                errors={formik.errors.year}
              />
            </BootstrapForm.Group>
            <BootstrapForm.Group as={Col} md={4}>
              <label htmlFor="time">Time</label>
              <MovieFormGroup
                name="time"
                value={formik.values.time}
                type="number"
                touched={formik.touched.time}
                errors={formik.errors.time}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={12}>
              <label htmlFor="time">Time</label>
              <MovieFormGroup
                name="imageUrl"
                value={formik.values.imageUrl}
                type="string"
                touched={formik.touched.imageUrl}
                errors={formik.errors.imageUrl}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={12}>
              <label htmlFor="director">Director</label>
              <MovieFormGroup
                name="director"
                value={formik.values.director}
                type="string"
                touched={formik.touched.director}
                errors={formik.errors.director}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={12}>
              <label htmlFor="writers">Writers</label>
              <MovieFormGroup
                name="writers"
                value={formik.values.writers}
                type="string"
                touched={formik.touched.writers}
                errors={formik.errors.writers}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={12}>
              <label htmlFor="stars">Stars</label>
              <MovieFormGroup
                name="stars"
                value={formik.values.stars}
                type="string"
                touched={formik.touched.stars}
                errors={formik.errors.stars}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <BootstrapForm.Row>
            <BootstrapForm.Group as={Col} md={12}>
              <label htmlFor="shortDEscription">Description</label>
              <MovieFormGroup
                name="shortDescription"
                value={formik.values.shortDescription}
                type="textbox"
                touched={formik.touched.shortDescription}
                errors={formik.errors.shortDescription}
              />
            </BootstrapForm.Group>
          </BootstrapForm.Row>
          <div className="form-group">
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!formik.isValid}
            >
              Submit
            </button>
            {console.log(JSON.stringify(formik.touched))}
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default MovieForm;
