import React from "react";
import { Col, Nav, Navbar, Row } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <Row>
      <Col>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">OMDB</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">
              Home
              {/* //as Link to work with router Home */}
            </Nav.Link>
            <Nav.Link as={Link} to="/create-movie">
              Create Movie
            </Nav.Link>
          </Nav>
        </Navbar>
      </Col>
    </Row>
  );
};

export default NavBar;
