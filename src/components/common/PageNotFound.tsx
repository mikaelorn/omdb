import React from "react";

const PageNotFound: React.FC = () => <h1>Oops! page not found</h1>;

export default PageNotFound;
