import React from "react";
import { useParams } from "react-router-dom";
import { useMovieQuery, patchMovie } from "../../api/movieService";
import { Movie } from "../../interfaces/Movie";
import { statusEnum } from "../../interfaces/Service";
import MovieForm from "../common/movieForm/MovieForm";

interface IEditMovie {
  id: string;
}

const EditMovie = () => {
  const { id } = useParams<IEditMovie>();
  const queryService = useMovieQuery(id);

  async function handleSubmit(movie: Movie) {
    await patchMovie(movie);
    alert("Updated!");
  }

  return (
    <div>
      {queryService.status === statusEnum.loading && <h2>Loading...</h2>}
      {queryService.status === statusEnum.error && (
        <h2>Error: {queryService.error}</h2>
      )}
      {queryService.status === statusEnum.loaded && (
        <MovieForm submit={handleSubmit} movie={queryService.payload} />
      )}
    </div>
  );
};

export default EditMovie;
