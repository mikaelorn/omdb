import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Route, Switch } from "react-router-dom";
import NavBar from "./components/common/NavBar";
import PageNotFound from "./components/common/PageNotFound";
import CreateMovie from "./components/CreateMovie";
import EditMovie from "./components/EditMovie";
import LandingPage from "./components/landingPage/landingPage";
import ReactDemo from "./components/reactDemo";
import FunctionalReactDemo from "./components/reactDemo/functionRef";

function App() {
  return (
    <Container fluid>
      <NavBar />
      <Row className="mt-2">
        <Col md={6} style={{ width: "100%", margin: "auto" }}>
          <Switch>
            <Route exact path="/" component={LandingPage} />
            <Route path="/react-demo" component={ReactDemo} />
            <Route path="/func-demo" component={FunctionalReactDemo} />
            <Route path="/create-movie" component={CreateMovie} />
            <Route path="/edit-movie/:id" component={EditMovie} />
            <Route component={PageNotFound} />
          </Switch>
        </Col>
      </Row>
    </Container>
  );
}

export default App;

//
