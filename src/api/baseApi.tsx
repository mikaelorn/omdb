import Axios from "axios";
import { BASE_API_URL } from "../constants";

export default Axios.create({
  baseURL: BASE_API_URL,
  responseType: "json",
  headers: {
    "Content-Type": "application/json",
  },
});
