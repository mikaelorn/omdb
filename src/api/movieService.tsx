import { useEffect, useState } from "react";
import { Movie } from "../interfaces/Movie";
import { BaseService, statusEnum } from "../interfaces/Service";
import Api from "./baseApi";

export const useMoviesQuery = () => {
  const [result, setResult] = useState<BaseService<Movie[]>>({
    status: statusEnum.loading,
  });

  useEffect(() => {
    //axios uses generic to type response body
    Api.get<Movie[]>("movies")
      .then((response) => {
        // handle success
        setResult({ status: statusEnum.loaded, payload: response.data });
      })
      .catch((error) => {
        // handle error
        setResult({ status: statusEnum.error, error: error });
      });
  }, []);

  return result;
};

export const useMovieQuery = (id: string) => {
  const [result, setResult] = useState<BaseService<Movie>>({
    status: statusEnum.loading,
  });

  useEffect(() => {
    //axios uses generic to type response body
    Api.get<Movie>("movies/" + id)
      .then((response) => {
        // handle success
        setResult({ status: statusEnum.loaded, payload: response.data });
      })
      .catch((error) => {
        // handle error
        setResult({ status: statusEnum.error, error: error });
      });
  }, [id]);

  return result;
};

export const patchMovie = async (movie: Movie) => {
  try {
    let result = await Api.patch<Movie>("movies/" + movie.id, movie);
    return result.data;
  } catch (error) {
    console.log("error");
    alert("Error patching movie!");
  }
};
export const createMovie = async (movie: Movie) => {
  try {
    let result = await Api.post<Movie>("movies", movie);
    return result.data;
  } catch (error) {
    console.log("error");
    alert("Error patching movie!");
  }
};

export const getServerData = async (): Promise<string> => {
  return new Promise((resolve) =>
    setTimeout(resolve, 3000, "hello from server")
  );
};