import * as yup from "yup";

export type Movie = {
  id?: number;
  time: number;
  director: string;
  writers: string;
  stars: string;
  imageUrl: string;
  shortDescription: string;
  name: string;
  year: number;
  type: string;
  rating: number;
};

export const movieSchema = yup.object({
  name: yup.string().required().min(3, "min 3 characters"),
  type: yup.string().required(),
  director: yup.string().required(),
  year: yup.number().required().min(1900),
  imageUrl: yup.string().required(),
  rating: yup.number().required().min(0).max(5),
  time: yup.number().required().min(5, "min 5 mins").max(240, "max 240 mins"),
  writers: yup.string().required(),
  stars: yup.string().required(),
  shortDesctription: yup.string().max(120),
});

export const initialMovieValues: Movie = {
  director: "",
  year: 2000,
  imageUrl: "",
  rating: 0,
  time: 0,
  writers: "",
  stars: "",
  shortDescription: "",
  name: "",
  type: "",
};
