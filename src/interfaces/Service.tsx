interface ServiceInit {
  status: statusEnum.init;
}
interface ServiceLoading {
  status: statusEnum.loading;
}
interface ServiceLoaded<T> {
  status: statusEnum.loaded;
  payload: T;
}
interface ServiceError {
  status: statusEnum.error;
  error: Error;
}
export type BaseService<T> =
  | ServiceInit
  | ServiceLoading
  | ServiceLoaded<T>
  | ServiceError;

export enum statusEnum {
  init = "init",
  loading = "loading",
  loaded = "loaded",
  error = "error",
}
